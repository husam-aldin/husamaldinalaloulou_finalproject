﻿using System;

namespace Final_Code_Project1
{
	public class EatingRoom
	{
		public static void EnterRoom ()
		{
			Console.WriteLine ("You are in the eating room\n");

			Console.WriteLine ("now you are in the eatingroom");
			Console.WriteLine ("i think you are living alone");
			Console.WriteLine ("right?");
			StartingPoint.answer = Console.ReadLine ();
			if (StartingPoint.answer == "right")
			{
				Console.WriteLine ("you have 3 options");
				Console.WriteLine ("search in the 'cupboard'");
				Console.WriteLine ("search in under the'eatingtable'");
				Console.WriteLine ("search in between the'boxes'");
			}


			Console.WriteLine ("press'startingpoint' to go to startingpoint");
			Console.WriteLine ("press 'livingroom' to go to living room");
			StartingPoint.RoomNav = Console.ReadLine ();
			switch (StartingPoint.RoomNav)
			{
			case("livingroom"):
				{
					
					Console.WriteLine ("you go to the livingroom");
					LivingRoom.EnterRoom ();
					break;
				}
			case("startingpoint"):
				{
					Console.WriteLine ("you go to the startingpoint");
					StartingPoint.Main ();
					break;
				}
			case("cupboard"):
				{
					Console.WriteLine ("you search in the cupboard but there are only old dishes");
					Console.WriteLine ("you should clean them next time");
					Console.WriteLine ("do not forget to clean them");
					EnterRoom();
					break;
				}
			case("eatingtable"):
				{
					Console.WriteLine ("you search under the table...it reminds you that you have to clean the room...");
					Console.WriteLine ("other than that nothing useful");
					Console.WriteLine ("i told you many times to clean ");
					Console.WriteLine ("next time i will not remind you");
					EnterRoom();
					break;
				}
			case("boxes"):
				{
					Console.WriteLine ("you search in the boxes but there are only old pictures");
					Console.WriteLine ("but wait whats that?");
					Console.WriteLine ("You find the golden key in there with the number '2'");
					EnterRoom();
					break;
				}
			default:
				Console.WriteLine ("wrong type again");
				EnterRoom ();
				break;
			}

		}
	}
}

